# rust-devops

### inject_charm_yaml 

This project takes in a local charm and injects it into a pre-existing YAML.
It expects the YAML is structured in a certain order to properly work.

*Yaml Example* `charm:` must precede `channel:`
```
mahrio-aws-cloudwatch:
  charm: mahrio-aws-cloudwatch
  channel: latest/beta
```
*Usage*
```
USAGE: inject_charm_yaml [/path/to/config.yaml.template] [/path/to/charm_dir/] [charm_filename.charm]
```
  