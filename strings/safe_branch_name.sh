#!/bin/bash
# Utilizing branch names can break Juju (eg: model names).. as well
# as be a malformed URL.. so if needing to use safe strings.. we run
# this routine first

UNSAFE_INPUTSTRING="$1"

SAFE_BRANCH_NAME=$(echo "$UNSAFE_INPUTSTRING"|tr '/' '-')
SAFE_BRANCH_NAME=$(echo "$SAFE_BRANCH_NAME"|tr '_' '-')

echo $SAFE_BRANCH_NAME
